﻿open System.Text.RegularExpressions
open System.IO
// For more information see https://aka.ms/fsharp-console-apps
printfn "Hello from F#"

let digitPattern = @"(\d|one|two|three|four|five|six|seven|eight|nine)"

let pattern1 = @$"{digitPattern}.*{digitPattern}"
let pattern2 = @$".*{digitPattern}.*"
let regex1 = new Regex(pattern1, RegexOptions.Compiled)
let regex2 = new Regex(pattern2, RegexOptions.Compiled)

let toNum (digit : string) : int =
    match digit with
    | "one" -> 1
    | "two" -> 2
    | "three" -> 3
    | "four" -> 4
    | "five" -> 5
    | "six" -> 6
    | "seven" -> 7
    | "eight" -> 8
    | "nine" -> 9
    | x ->  int x

let getNumber (str : string) : int =    
    let matches1 = regex1.Matches str
    let matches2 = regex2.Matches str

    if matches1.Count > 0 then
        let grps = (matches1.Item 0).Groups
        
        match grps.Count with        
        | 3 ->  (toNum (grps.Item 1).Value) * 10 + toNum (grps.Item 2).Value
        | _ -> 0            
    else        
        if matches2.Count > 0 then
            let grps = (matches2.Item 0).Groups        
            match grps.Count with        
            | 2 ->  (toNum (grps.Item 1).Value) * 10 + toNum (grps.Item 1).Value
            | _ -> 0
        else 
            0

let readInput (path : string) : string seq =
    File.ReadLines path

readInput @"input"
|> Seq.map getNumber
|> Seq.sum
|> printfn "%i"